package me.ddevil.core.utils.players;

import org.json.simple.JSONArray;
import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Callable;

/**
 * Created by BRUNO II on 23/06/2016.
 */
public class PreviousNamesFetcher implements Callable<List<String>> {
    private static final String PROFILE_URL = "https://api.mojang.com/user/profiles/";
    private static final String SUFFIX = "/names";
    private final JSONParser jsonParser = new JSONParser();
    private final UUID uuid;

    public PreviousNamesFetcher(UUID uuid) {
        this.uuid = uuid;
    }

    @Override
    public List<String> call() throws IOException, ParseException {
        URL url = new URL(PROFILE_URL + uuid.toString().replace("-", "") + SUFFIX);
        String urlHost = url.toString();
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        InputStreamReader input = new InputStreamReader(connection.getInputStream());
        JSONAware parse = (JSONAware) jsonParser.parse(input);
        if (parse == null) {
            throw new IllegalStateException("null response");
        }
        if (!(parse instanceof JSONArray)) {
            JSONObject response = (JSONObject) parse;
            String cause = (String) response.get("error");
            String errorMessage = (String) response.get("errorMessage");
            if (cause != null && cause.length() > 0) {
                throw new IllegalStateException(cause + ": " + errorMessage);
            }
        }
        ArrayList<String> names = new ArrayList();
        JSONArray response = (JSONArray) parse;
        for (int i = 0; i < response.size(); i++) {
            names.add((String) ((JSONObject) response.get(i)).get("name"));
        }
        return names;
    }
}
