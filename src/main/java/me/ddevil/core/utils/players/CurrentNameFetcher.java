package me.ddevil.core.utils.players;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.UUID;
import java.util.concurrent.Callable;

public class CurrentNameFetcher implements Callable<String> {
    private static final String PROFILE_URL = "https://sessionserver.mojang.com/session/minecraft/profile/";
    private final JSONParser jsonParser = new JSONParser();
    private final UUID uuid;

    public CurrentNameFetcher(UUID uuid) {
        this.uuid = uuid;
    }

    @Override
    public String call() throws IOException, ParseException {

        HttpURLConnection connection = (HttpURLConnection) new URL(PROFILE_URL + uuid.toString().replace("-", "")).openConnection();
        JSONObject response;
        try {
            response = (JSONObject) jsonParser.parse(new InputStreamReader(connection.getInputStream()));
        } catch (IOException e) {
            throw new IllegalStateException("TooManyRequestsException: The client has sent too many requests within a certain amount of time");
        }
        String name = (String) response.get("name");

        if (name == null) {
            String cause = (String) response.get("error");
            String errorMessage = (String) response.get("errorMessage");
            if (cause != null && cause.length() > 0) {
                throw new IllegalStateException(cause + ": " + errorMessage);
            }
        }
        return name;
    }
}