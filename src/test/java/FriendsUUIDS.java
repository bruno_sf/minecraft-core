

import java.util.UUID;

/**
 * Created by BRUNO II on 23/06/2016.
 */
public class FriendsUUIDS {
    public static final UUID BLESSED = UUID.fromString("360fe882-d33b-422f-bb70-1b63a92ba540");
    public static final UUID DDEVIL = UUID.fromString("a9c90643-cf6f-4f86-929c-4071ec054039");
    public static final UUID NIKESKI = UUID.fromString("71471d10-a2d0-44ab-b4cf-7875b1e6c7cd");
    public static final UUID ARKENUM = UUID.fromString("af0d9c2d-a447-4705-9628-b1daa29bab1e");

}
